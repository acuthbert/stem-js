TODO
====

* HAVING support
* Logging
* Error handling
* indexes
* partial updates
* relationships
* enum columns
* decimal and money columns
* one of filter
* not filter
* top level aggregates
* batch updates
* delete all
* timezones
* aggregate types (i.e. presently average on an int column is coming out as a int even if it is decimal)
