"use strict";
exports.__esModule = true;
var Store = /** @class */ (function () {
    function Store() {
        this.repositories = [];
    }
    Store.prototype.addRepository = function (repository) {
        repository.setStore(this);
        this.repositories.push(repository);
    };
    Store.prototype.getRepositories = function () {
        return this.repositories;
    };
    return Store;
}());
exports.Store = Store;
//# sourceMappingURL=Store.js.map