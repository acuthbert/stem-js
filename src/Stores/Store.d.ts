import { Entity } from '../Entities/Entity';
export declare abstract class Store {
    constructor();
    abstract getSingleEntity<T extends Entity>(c: {
        new (): T;
    }, id: string | number): Promise<T>;
    abstract storeEntity<T extends Entity>(entity: T): Promise<T>;
}
