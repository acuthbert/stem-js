import { Entity } from '../../Entities/Entity';
import { Filter } from '../../Collections/Filters/Filter';
import { EqualsFilter } from '../../Collections/Filters/EqualsFilter';
import { GreaterThanFilter } from '../../Collections/Filters/GreaterThanFilter';
import { AndGroupFilter } from '../../Collections/Filters/AndGroupFilter';
import { OrGroupFilter } from '../../Collections/Filters/OrGroupFilter';
import { LessThanFilter } from '../../Collections/Filters/LessThanFilter';
import { StartsWithFilter } from '../../Collections/Filters/StartsWithFilter';
import {EndsWithFilter} from '../../Collections/Filters/EndsWithFilter';
import {ContainsFilter} from '../../Collections/Filters/ContainsFilter';

export class MemoryFilterProcessor {
  filter(filter: Filter, items: Entity[]): Entity[] {
    let toRemove: Entity[] = [];

    if (filter instanceof AndGroupFilter) {
      const filters = filter.getFilters();

      for (const childFilter of filters) {
        const childProcessor = new MemoryFilterProcessor();
        const childItemsToRemove = childProcessor.filter(childFilter, items);

        toRemove.push(...childItemsToRemove);
      }

    } else if (filter instanceof OrGroupFilter) {
      const filters = filter.getFilters();
      let first = true;

      for (const childFilter of filters) {
        const childProcessor = new MemoryFilterProcessor();
        const childItemsToRemove = childProcessor.filter(childFilter, items);

        if (first) {
          toRemove.push(...childItemsToRemove);
        } else {
          toRemove = toRemove.filter((a) => {
            let inChild = false;
            childItemsToRemove.forEach((b) => {
              if (b.id === a.id) {
                inChild = true;
              }
            });

            return !inChild;
          });
          for (const childItem of childItemsToRemove) {

          }
        }

        first = false;
      }

    } else if (filter instanceof EqualsFilter) {
      for (const item of items) {
        if (item[filter.column] !== filter.value) {
          toRemove.push(item);
        }
      }
    } else if (filter instanceof GreaterThanFilter) {
      for (const item of items) {
        if (filter.inclusive) {
          if (item[filter.column] < filter.value) {
            toRemove.push(item);
          }
        } else {
          if (item[filter.column] <= filter.value) {
            toRemove.push(item);
          }
        }
      }
    } else if (filter instanceof LessThanFilter) {
      for (const item of items) {
        if (filter.inclusive) {
          if (item[filter.column] > filter.value) {
            toRemove.push(item);
          }
        } else {
          if (item[filter.column] >= filter.value) {
            toRemove.push(item);
          }
        }
      }
    } else if (filter instanceof StartsWithFilter) {
      for (const item of items) {
        if (item[filter.column].toLowerCase().indexOf(filter.value.toLowerCase()) !== 0) {
          toRemove.push(item);
        }
      }
    } else if (filter instanceof EndsWithFilter) {
      for (const item of items) {
        if (item[filter.column].toLowerCase().indexOf(filter.value.toLowerCase())
            !== (item[filter.column].length - 1)) {
          toRemove.push(item);
        }
      }
    } else if (filter instanceof ContainsFilter) {
      for (const item of items) {
        if (item[filter.column].toLowerCase().indexOf(filter.value.toLowerCase()) === 0) {
          toRemove.push(item);
        }
      }
    }

    return toRemove;
  }
}
