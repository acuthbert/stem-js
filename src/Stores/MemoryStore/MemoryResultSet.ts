import { ResultSet } from '../ResultSet';
import { Entity } from '../../Entities/Entity';

export class MemoryResultSet<T extends Entity> extends ResultSet<T>{
  private items: T[];

  constructor(items: T[]) {
    super();
    this.items = items;
    this.totalLength = items.length;
  }

  public [Symbol.iterator](): Iterator<T> {
    let pointer = -1;
    const items = this.items;
    return {
      next(): IteratorResult<T> {
        if (pointer < items.length - 1) {
          pointer = pointer + 1;
          return {
            done: false,
            value: items[pointer],
          };
        }
        return {
          done: true,
          /// @ts-ignore
          value: null,
        };
      },
    };
  }

  item(index: number): T {
    return this.items[index];
  }
}
