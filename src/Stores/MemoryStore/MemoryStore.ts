import { Entity } from '../../Entities/Entity';
import { Store } from '../Store';
import { EntityNotFoundError } from '../Errors/EntityNotFoundError';
import { Repository } from '../../Repositories/Repository';
import { ResultSet } from '../ResultSet';
import { Filter } from '../../Collections/Filters/Filter';
import { MemoryFilterProcessor } from './MemoryFilterProcessor';
import { MemoryResultSet } from './MemoryResultSet';
import { Sort, SortDirection } from '../../Collections/Sort';
import { Join } from '../../Collections/Joins/Join';
import { JoinType } from '../../Collections/Joins/JoinType';
import { Aggregate } from '../../Collections/Aggregates/Aggregate';

export class MemoryStore extends Store {

  private entities: {[key: string]: {[key: string]: Entity|any}} = {};

  private autoIds: {[key: string]: number} = {};

  async getSingleEntity<T extends Entity>(c: new() => T, id: string|number): Promise<T> {
    const entityType = c.name;

    if (!this.entities[entityType] || !this.entities[entityType][id]) {
      throw new EntityNotFoundError(id);
    }

    return this.entities[entityType][id.toString()];
  }

  async storeEntity<T extends Entity>(entity: T, repository: Repository<T>) {
    const entityType = entity.constructor.name;

    if (!entity.id) {
      if (!this.autoIds[entityType]) {
        this.autoIds[entityType] = 0;
      }

      this.autoIds[entityType] = this.autoIds[entityType] + 1;
      entity.id = this.autoIds[entityType];
    }

    if (!this.entities[entityType]) {
      this.entities[entityType] = [];
    }

    this.entities[entityType][entity.id.toString()] = entity;
    return entity;
  }

  async fetchItems<T extends Entity> (
      repository: Repository<T>,
      filter: Filter,
      sorts: Sort[],
      groups: string[],
      intersections: Join[],
      aggregates: Aggregate[]): Promise<ResultSet<T>> {

    const entityType = (repository.getEntityType()).name;

    let items = (this.entities[entityType]) ?
        Object.values(this.entities[entityType]) : [];

    const processor = new MemoryFilterProcessor();
    const toRemove = processor.filter(filter, items);

    for (const item of toRemove) {
      items = items.filter(a => a.id !== item.id);
    }

    let sortedItems = items;

    sortedItems = sortedItems.sort((a, b) => {
      for (const sort of sorts) {

        if (a[sort.column] === b[sort.column]) {
          continue;
        }

        if (sort.direction === SortDirection.Ascending) {
          return a[sort.column] > b[sort.column] ? 1 : -1;
        }

        return a[sort.column] < b[sort.column] ? 1 : -1;
      }

      return 0;
    });

    for (const intersection of intersections) {
      intersection.collection.invalidate();
      await intersection.collection.fetch();

      sortedItems = sortedItems.filter((item) => {
        let foundRow = null;
        for (const child of intersection.collection) {
          if (child[intersection.targetColumn] === item[intersection.sourceColumn]) {
            foundRow = child;

            // Handle pull ups
            for (const pullUpSource of Object.keys(intersection.pullUps)) {
              const pullUpAlias = intersection.pullUps[pullUpSource];

              item[pullUpAlias] = child[pullUpSource];
            }
          }
        }

        if (intersection.joinType === JoinType.Intersection) {
          return foundRow !== null;
        }

        return true;
      });
    }

    const aggregateSets: {[alias: string]: any} = {};

    // Aggregates Pass 1: Calculate the values
    aggregates.forEach((aggregate) => {
      aggregateSets[aggregate.alias] = aggregate.getAggregatedValues(sortedItems, groups);
    });

    // Handle groups
    if (groups.length > 0) {
      const dupes: string[] = [];
      sortedItems = sortedItems.filter((item) => {
        const groupKey = this.getGroupKeyForEntity(item, groups);
        if (dupes.includes(groupKey)) {
          return false;
        }

        dupes.push(groupKey);
        return true;
      });
    }

    if (aggregates.length > 0) {
      for (const item of sortedItems) {
        aggregates.forEach((aggregate) => {
          const groupKey = Aggregate.getGroupKey(item, groups);
          item[aggregate.alias] = aggregateSets[aggregate.alias][groupKey];
        });
      }
    }

    return new MemoryResultSet<T>(sortedItems);
  }

  private getGroupKeyForEntity(item: Entity, groups: string[]): string {
    return groups.reduce((groupKey, group) => `${groupKey}|${item[group]}`, '');
  }

  async deleteEntity<T extends Entity>(entity: T, repository: Repository<T>): Promise<void> {
    delete this.entities[entity.constructor.name][entity.id];
  }
}
