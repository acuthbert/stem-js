import {Repository} from '../Repositories/Repository';
import {Entity} from '../Entities/Entity';

export interface StoreWithRigidSchemaInterface {
  checkSchema(repository: Repository<Entity>): void;
}
