import { Entity } from '../Entities/Entity';
import { Repository } from '../Repositories/Repository';
import { ResultSet } from './ResultSet';
import { Filter } from '../Collections/Filters/Filter';
import { Sort } from '../Collections/Sort';
import { Join } from '../Collections/Joins/Join';
import { Aggregate } from '../Collections/Aggregates/Aggregate';

export abstract class Store {

  abstract async getSingleEntity<T extends Entity>(c: {new(): T; }, id: string|number): Promise<T>;

  abstract async storeEntity<T extends Entity>(entity: T, repository: Repository<T>): Promise<T>;

  abstract async deleteEntity<T extends Entity>(entity: T, repository: Repository<T>): Promise<void>;

  abstract async fetchItems<T extends Entity>(
    repository: Repository<T>,
    filter: Filter,
    sorts: Sort[],
    groups: string[],
    intersections: Join[],
    aggregates: Aggregate[]): Promise<ResultSet<T>>;

  private repositories: Repository<Entity>[] = [];

  public addRepository(repository: Repository<Entity>) {
    repository.setStore(this);
    this.repositories.push(repository);
  }

  public getRepositories(): Repository<Entity>[] {
    return this.repositories;
  }
}
