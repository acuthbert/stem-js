export declare class EntityNotFoundError extends Error {
    private id?;
    constructor(id?: string | number);
}
