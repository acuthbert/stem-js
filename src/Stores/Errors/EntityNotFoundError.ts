export class EntityNotFoundError extends Error {

  private id?: string | number;

  constructor(id?: string|number) {
    super('An entity could not be loaded.');

    Object.setPrototypeOf(this, EntityNotFoundError.prototype);

    this.name = this.constructor.name;
    this.id = id;
  }
}
