import { Entity } from '../../Entities/Entity';

export class CantStoreEntityError extends Error {
  constructor(message: string, public entity: Entity) {
    super(message);
  }
}
