import { MySqlFilter } from './MySqlFilter';
import { AndGroupFilter } from '../../../Collections/Filters/AndGroupFilter';
import { MySqlStore } from '../MySqlStore';
import { SqlWhereExpression } from '../SqlTokens/SqlWhereExpression';
import { SqlWhereExpressionGroup } from '../SqlTokens/SqlWhereExpressionGroup';
import { BooleanType } from '../../../Shared/BooleanType';

export class MySqlAndGroupFilter extends MySqlFilter<AndGroupFilter> {
  getWhereExpression (): SqlWhereExpression {
    const filters = this.filter.getFilters();

    const whereExpression = new SqlWhereExpressionGroup();
    whereExpression.booleanOperator = BooleanType.And;

    if (!filters.length) {
      return whereExpression;
    }

    for (const filter of filters) {
      const mysqlFilter = MySqlStore.getMappedFilter(filter);
      const filterResult = mysqlFilter.getWhereExpression();
      whereExpression.expressions.push(filterResult);
    }

    return whereExpression;
  }
}
