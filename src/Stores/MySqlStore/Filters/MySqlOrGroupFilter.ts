import { MySqlFilter } from './MySqlFilter';
import { MySqlStore } from '../MySqlStore';
import { OrGroupFilter } from '../../../Collections/Filters/OrGroupFilter';
import { SqlWhereExpression } from '../SqlTokens/SqlWhereExpression';
import { SqlWhereExpressionGroup } from '../SqlTokens/SqlWhereExpressionGroup';
import { BooleanType } from '../../../Shared/BooleanType';

export class MySqlOrGroupFilter extends MySqlFilter<OrGroupFilter> {
  getWhereExpression (): SqlWhereExpression {
    const filters = this.filter.getFilters();

    const whereExpression = new SqlWhereExpressionGroup();
    whereExpression.booleanOperator = BooleanType.Or;

    if (!filters.length) {
      return whereExpression;
    }

    for (const filter of filters) {
      const mysqlFilter = MySqlStore.getMappedFilter(filter);
      const filterResult = mysqlFilter.getWhereExpression();
      whereExpression.expressions.push(filterResult);
    }

    return whereExpression;
  }
}
