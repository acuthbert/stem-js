import { MySqlFilter } from './MySqlFilter';
import { ContainsFilter } from '../../../Collections/Filters/ContainsFilter';
import { SqlWhereExpression } from '../SqlTokens/SqlWhereExpression';
import { SqlWhereExpressionLiteral } from '../SqlTokens/SqlWhereExpressionLiteral';

export class MySqlContainsFilter extends MySqlFilter<ContainsFilter> {
  getWhereExpression (): SqlWhereExpression {

    const expression = new SqlWhereExpressionLiteral();
    expression.expression = `${this.filter.column} LIKE '%?%'`;
    expression.params.push(this.filter.value);

    return expression;
  }
}
