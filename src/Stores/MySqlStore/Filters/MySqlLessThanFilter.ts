import { MySqlFilter } from './MySqlFilter';
import { LessThanFilter } from '../../../Collections/Filters/LessThanFilter';
import { SqlWhereExpression } from '../SqlTokens/SqlWhereExpression';
import { SqlWhereExpressionLiteral } from '../SqlTokens/SqlWhereExpressionLiteral';

export class MySqlLessThanFilter extends MySqlFilter<LessThanFilter> {
  getWhereExpression (): SqlWhereExpression {

    const operator = this.filter.inclusive ? '>=' : '>';

    const expression = new SqlWhereExpressionLiteral();
    expression.expression = `${this.filter.column} ${operator} ?`;
    expression.params.push(this.filter.value);

    return expression;
  }
}
