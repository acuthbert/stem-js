import { MySqlFilter } from './MySqlFilter';
import { StartsWithFilter } from '../../../Collections/Filters/StartsWithFilter';
import { SqlWhereExpression } from '../SqlTokens/SqlWhereExpression';
import { SqlWhereExpressionLiteral } from '../SqlTokens/SqlWhereExpressionLiteral';

export class MySqlStartsWithFilter extends MySqlFilter<StartsWithFilter> {
  getWhereExpression (): SqlWhereExpression {
    const expression = new SqlWhereExpressionLiteral();
    expression.expression = `${this.filter.column} LIKE '?%'`;
    expression.params.push(this.filter.value);
    return expression;
  }
}
