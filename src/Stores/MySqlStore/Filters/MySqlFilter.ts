import { Filter } from '../../../Collections/Filters/Filter';
import {SqlWhereExpression} from "../SqlTokens/SqlWhereExpression";

export abstract class MySqlFilter<T extends Filter> {
  protected filter: T;

  constructor(filter: T) {
    this.filter = filter;
  }

  abstract getWhereExpression(): SqlWhereExpression;
}
