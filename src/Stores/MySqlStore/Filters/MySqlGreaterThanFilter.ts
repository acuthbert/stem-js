import { GreaterThanFilter } from '../../../Collections/Filters/GreaterThanFilter';
import { MySqlFilter } from './MySqlFilter';
import { SqlWhereExpression } from '../SqlTokens/SqlWhereExpression';
import { SqlWhereExpressionLiteral } from '../SqlTokens/SqlWhereExpressionLiteral';

export class MySqlGreaterThanFilter extends MySqlFilter<GreaterThanFilter> {
  getWhereExpression (): SqlWhereExpression {
    const operator = this.filter.inclusive ? '>=' : '>';
    const expression = new SqlWhereExpressionLiteral();
    expression.expression = `${this.filter.column} ${operator} ?`;
    expression.params.push(this.filter.value);

    return  expression;
  }
}
