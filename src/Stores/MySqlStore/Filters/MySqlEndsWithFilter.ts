import { MySqlFilter } from './MySqlFilter';
import { EndsWithFilter } from '../../../Collections/Filters/EndsWithFilter';
import { SqlWhereExpression } from '../SqlTokens/SqlWhereExpression';
import { SqlWhereExpressionLiteral } from '../SqlTokens/SqlWhereExpressionLiteral';

export class MySqlEndsWithFilter extends MySqlFilter<EndsWithFilter> {
  getWhereExpression (): SqlWhereExpression {

    const expression = new SqlWhereExpressionLiteral();
    expression.expression = `${this.filter.column} LIKE '%?'`;
    expression.params.push(this.filter.value);

    return expression;
  }
}
