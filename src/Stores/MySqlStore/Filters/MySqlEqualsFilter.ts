import { MySqlFilter } from './MySqlFilter';
import { EqualsFilter } from '../../../Collections/Filters/EqualsFilter';
import { SqlWhereExpression } from '../SqlTokens/SqlWhereExpression';
import { SqlWhereExpressionLiteral } from '../SqlTokens/SqlWhereExpressionLiteral';

export class MySqlEqualsFilter extends MySqlFilter<EqualsFilter> {
  getWhereExpression (): SqlWhereExpression {
    const expression = new SqlWhereExpressionLiteral();
    expression.expression = `${this.filter.column} = ?`;
    expression.params.push(this.filter.value);
    return expression;
  }
}
