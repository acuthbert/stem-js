import { MySqlColumnSchema } from './MySqlColumnSchema';
import { Dayjs } from 'dayjs';
import dayjs = require('dayjs');

export class MySqlDateColumn extends MySqlColumnSchema {

  getSchemaSql (): string {
    return 'date';
  }

  transformDataForStorage(data: any): any {
    if (data.format) {
      return data.format('YYYY-MM-DD');
    }

    if (data instanceof Date) {
      return dayjs(data).format('YYYY-MM-DD');
    }

    return super.transformDataForStorage(data);
  }

  trasformDataFromStorage(data: any): any {
    return dayjs(data);
  }
}
