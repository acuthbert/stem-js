import { MySqlColumnSchema } from './MySqlColumnSchema';

export class MySqlBooleanColumn extends MySqlColumnSchema {
  getSchemaSql (): string {
    return 'tinyint(1)';
  }

  transformDataForStorage (data: any): any {
    return (data ? 1 : 0);
  }

  trasformDataFromStorage (data: any): any {
    return (data === 1);
  }
}
