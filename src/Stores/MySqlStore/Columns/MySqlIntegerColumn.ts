import { MySqlColumnSchema } from './MySqlColumnSchema';
import { isNumeric } from 'tslint';

export class MySqlIntegerColumn extends MySqlColumnSchema {
  getSchemaSql(): string {
    return 'int(11)';
  }

  trasformDataFromStorage(data: any): any {
    if (typeof data === 'number') {
      return data;
    }

    return parseInt(data, 10);
  }
}
