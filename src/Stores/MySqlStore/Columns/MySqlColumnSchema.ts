import { Column } from '../../../Schema/Columns/Column';

export abstract class MySqlColumnSchema {
  protected baseColumn: Column;

  constructor(baseColumn: Column) {
    this.baseColumn = baseColumn;
  }

  abstract getSchemaSql(): string;

  transformDataForStorage(data: any): any {
    return data;
  }

  trasformDataFromStorage(data: any): any {
    return data;
  }
}
