import { MySqlColumnSchema } from './MySqlColumnSchema';

export class MySqlStringColumn extends MySqlColumnSchema {
  getSchemaSql(): string {
    return `VARCHAR(${this.baseColumn.length})`;
  }
}
