import { MySqlColumnSchema } from './MySqlColumnSchema';
import { Dayjs } from 'dayjs';
import dayjs = require('dayjs');

export class MySqlDateTimeColumn extends MySqlColumnSchema {

  getSchemaSql (): string {
    return 'datetime';
  }

  transformDataForStorage(data: any): any {
    if (data.format) {
      return data.format('YYYY-MM-DD HH:mm:ss');
    }

    if (data instanceof Date) {
      return dayjs(data).format('YYYY-MM-DD HH:mm:ss');
    }

    return super.transformDataForStorage(data);
  }

  trasformDataFromStorage(data: any): any {
    return dayjs(data);
  }
}
