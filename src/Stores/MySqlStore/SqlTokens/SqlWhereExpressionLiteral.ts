import { SqlWhereExpression } from './SqlWhereExpression';

export class SqlWhereExpressionLiteral extends SqlWhereExpression {
  public expression: string = '';
  public params: any[] = [];

  getSql(): [string, any[]] {
    return [`${this.expression}`, this.params];
  }
}
