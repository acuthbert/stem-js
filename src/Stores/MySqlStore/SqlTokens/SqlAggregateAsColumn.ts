import { SqlToken } from './SqlToken';

export class SqlAggregateAsColumn extends SqlToken {

  constructor(public aggregateExpression: string, public alias: string) {
    super();
  }

  getSql(): [string, any[]] {
    return [`${this.aggregateExpression} AS \`${this.alias}\``, []];
  }
}
