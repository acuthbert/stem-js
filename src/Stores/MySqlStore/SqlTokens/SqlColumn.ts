import { SqlToken } from './SqlToken';

export class SqlColumn extends SqlToken {

  public statementAlias: string = '';

  public columnName: string = '';

  public alias: string = '';

  getSql(): [string, any[]] {
    return [`\`${this.statementAlias}\`.\`${this.columnName}\` as \`${this.alias}\``, []];
  }
}
