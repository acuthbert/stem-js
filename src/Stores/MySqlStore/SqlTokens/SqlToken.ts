export abstract class SqlToken {
  abstract getSql(): [string, any[]];
}
