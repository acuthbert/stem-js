import { SqlWhereExpression } from './SqlWhereExpression';
import { BooleanType } from '../../../Shared/BooleanType';

export class SqlWhereExpressionGroup extends SqlWhereExpression {
  public expressions: SqlWhereExpression[] = [];
  public booleanOperator: BooleanType = BooleanType.And;

  getSql(): [string, any[]] {
    const expressionSqls = [];
    const params = [];
    for (const expression of this.expressions) {
      const [expressionSql, expressionParams] = expression.getSql();
      expressionSqls.push(expressionSql);
      params.push(...expressionParams);
    }

    if (expressionSqls.length > 0){
      const sql = expressionSqls.join(this.booleanOperator === BooleanType.And ? 'AND' : 'OR');
      return [`(${expressionSqls})`, params];
    }

    return ['', []];
  }
}
