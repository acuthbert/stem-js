import { SqlToken } from './SqlToken';
import { SortDirection } from '../../../Collections/Sort';

export class SqlOrderBy extends SqlToken {
  public column: string = '';
  public direction: SortDirection = SortDirection.Ascending;

  getSql(): [string, any[]] {
    const directionStatement = (this.direction === SortDirection.Ascending) ? 'ASC' : 'DESC';
    return [`\`${this.column}\` ${directionStatement}`, []];
  }

}
