import { SqlToken } from './SqlToken';
import { SqlStatement } from './SqlStatement';
import { JoinType } from '../../../Collections/Joins/JoinType';

export class SqlJoin extends SqlToken {
  public parentStatementAlias: string = '';
  public statement: SqlStatement;
  public onSource: string = '';
  public onDestination: string = '';
  public joinType: JoinType = JoinType.Intersection;
  public pullUps: {[index: string]: string} = {};

  constructor(statement: SqlStatement){
    super();
    this.statement = statement;
  }

  getSql(): [string, any[]] {
    if (this.statement) {
      const typeSql = (this.joinType === JoinType.Intersection) ? 'INNER' : 'LEFT';
      const [subSql, subParams] = this.statement.getSql();
      // tslint:disable-next-line:max-line-length
      return [` ${typeSql} JOIN (${subSql}) AS \`${this.statement.alias}\`
        ON \`${this.parentStatementAlias}\`.\`${this.onSource}\` =
        \`${this.statement.alias}\`.\`${this.onDestination}\``, subParams];
    }

    return ['', []];
  }
}
