import { SqlToken } from './SqlToken';

export class SqlGroup extends SqlToken {

  public column: string = '';

  getSql(): [string, any[]] {
    return [`\`${this.column}\``, []];
  }
}
