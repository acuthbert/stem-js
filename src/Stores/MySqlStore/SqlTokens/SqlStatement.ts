import { SqlToken } from './SqlToken';
import { SqlOrderBy } from './SqlOrderBy';
import { SqlWhereExpression } from './SqlWhereExpression';
import { SqlColumn } from './SqlColumn';
import { SqlGroup } from './SqlGroup';
import { SqlJoin } from './SqlJoin';
import {SqlAggregateAsColumn} from './SqlAggregateAsColumn';

export class SqlStatement extends SqlToken {
  public columns: SqlColumn[] = [];
  public aggregates: SqlAggregateAsColumn[] = [];
  public tableName: string = '';
  public joins: SqlJoin[] = [];
  public sorts: SqlOrderBy[] = [];
  public groups: SqlGroup[] = [];
  public whereExpression?: SqlWhereExpression;
  private _alias: string = '';
  private statementNumber = 0;

  static statementCount = 0;

  get alias(): string {
    if (this._alias === '') {
      if (!this.statementNumber) {
        this.statementNumber = SqlStatement.statementCount = SqlStatement.statementCount + 1;
      }

      return `${this.tableName}_${this.statementNumber}`;
    }

    return this._alias;
  }

  set alias(value: string) {
    this._alias = value;
  }

  getSql(): [string, any[]] {

    let sql = 'SELECT ';

    const columnSqls = [];
    for (const column of this.columns) {
      columnSqls.push(column.getSql()[0]);
    }

    for (const aggregate of this.aggregates) {
      columnSqls.push(aggregate.getSql()[0]);
    }

    sql += columnSqls.join(', ');
    sql += ` FROM \`${this.tableName}\` AS \`${this.alias}\``;

    const joinSqls = [];
    for (const join of this.joins) {
      joinSqls.push(join.getSql()[0]);
    }

    if (joinSqls.length > 0) {
      sql += joinSqls.join(' ');
    }

    let params = [];
    if (this.whereExpression) {
      const [whereExpressionSql, whereExpressionParams] = this.whereExpression.getSql();

      if (whereExpressionSql.trim().length > 0) {
        sql += ` WHERE ${whereExpressionSql}`;
        params = whereExpressionParams;
      }
    }

    const groupSqls = [];

    for (const group of this.groups) {
      groupSqls.push(group.getSql()[0]);
    }

    if (groupSqls.length > 0) {
      sql += ` GROUP BY ${groupSqls.join(', ')}`;
    }

    const sortSqls = [];
    for (const sort of this.sorts) {
      sortSqls.push(sort.getSql()[0]);
    }

    if (sortSqls.length > 0) {
      sql += ` ORDER BY ${sortSqls.join(', ')}`;
    }

    return [sql, params];
  }
}
