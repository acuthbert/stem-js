export class MySqlConnectionSettings {
  password = '';
  database = '';
  host = '';
  port = 3306;
  user = '';
}
