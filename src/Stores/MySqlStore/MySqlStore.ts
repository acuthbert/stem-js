import { Store } from '../Store';
import { Entity } from '../../Entities/Entity';
// @ts-ignore
import { Connection, FieldPacket, ResultSetHeader, RowDataPacket } from 'mysql2/promise';
import { StoreWithRigidSchemaInterface } from '../StoreWithRigidSchemaInterface';
import { Repository } from '../../Repositories/Repository';
import { Column } from '../../Schema/Columns/Column';
import { ComparisonTableSchema } from './ComparisonTableSchema';
import { Filter } from '../../Collections/Filters/Filter';
import { ResultSet } from '../ResultSet';
import { MySqlResultSet } from './MySqlResultSet';
import { MySqlColumnSchema } from './Columns/MySqlColumnSchema';
import { MySqlIntegerColumn } from './Columns/MySqlIntegerColumn';
import { MySqlStringColumn } from './Columns/MySqlStringColumn';
import { MySqlBooleanColumn } from './Columns/MySqlBooleanColumn';
import { MySqlDateColumn } from './Columns/MySqlDateColumn';
import { MySqlFilter } from './Filters/MySqlFilter';
import { MySqlEqualsFilter } from './Filters/MySqlEqualsFilter';
import { MySqlAndGroupFilter } from './Filters/MySqlAndGroupFilter';
import { MySqlGreaterThanFilter } from './Filters/MySqlGreaterThanFilter';
import { MySqlLessThanFilter } from './Filters/MySqlLessThanFilter';
import { MySqlStartsWithFilter } from './Filters/MySqlStartsWithFilter';
import { MySqlEndsWithFilter } from './Filters/MySqlEndsWithFilter';
import { MySqlContainsFilter } from './Filters/MySqlContainsFilter';
import { MySqlConnectionSettings } from './MySqlConnectionSettings';
import { Sort } from '../../Collections/Sort';
import { MySqlOrGroupFilter } from './Filters/MySqlOrGroupFilter';
import { Join } from '../../Collections/Joins/Join';
import { SqlStatement } from './SqlTokens/SqlStatement';
import { SqlColumn } from './SqlTokens/SqlColumn';
import { SqlOrderBy } from './SqlTokens/SqlOrderBy';
import { SqlGroup } from './SqlTokens/SqlGroup';
import { SqlJoin } from './SqlTokens/SqlJoin';
import { Pool } from 'mysql';
import { MySqlSumOfAggregate } from './Aggregates/MySqlSumOfAggregate';
import { Aggregate } from '../../Collections/Aggregates/Aggregate';
import { MySqlAggregate } from './Aggregates/MySqlAggregate';
import { MySqlAverageAggregate } from './Aggregates/MySqlAverageAggregate';
import { CantStoreEntityError } from '../Errors/CantStoreEntityError';
import { MySqlDateTimeColumn } from './Columns/MySqlDateTimeColumn';
import { MySqlMaximumAggregate } from './Aggregates/MySqlMaximumAggregate';
import { MySqlMinimumAggregate } from './Aggregates/MySqlMinimumAggregate';
import { MySqlCountAggregate } from './Aggregates/MySqlCountAggregate';
import {MySqlCountDistinctAggregate} from './Aggregates/MySqlCountDistinctAggregate';

const mysql = require('mysql2/promise');

export class MySqlStore extends Store implements StoreWithRigidSchemaInterface {

  private connectionSettings: MySqlConnectionSettings;
  private connection?: Connection;

  public static columnSchemaMappings: {[index: string]: any} = {
    IntegerColumn: MySqlIntegerColumn,
    StringColumn: MySqlStringColumn,
    BooleanColumn: MySqlBooleanColumn,
    DateColumn: MySqlDateColumn,
    DateTimeColumn: MySqlDateTimeColumn,
  };

  public static filterMappings: {[index: string]: any} = {
    Collection: MySqlAndGroupFilter,
    AndGroupFilter: MySqlAndGroupFilter,
    OrGroupFilter: MySqlOrGroupFilter,
    EqualsFilter: MySqlEqualsFilter,
    GreaterThanFilter: MySqlGreaterThanFilter,
    LessThanFilter: MySqlLessThanFilter,
    StartsWithFilter: MySqlStartsWithFilter,
    EndsWithFilter: MySqlEndsWithFilter,
    ContainsFilter: MySqlContainsFilter,
  };

  public static aggregateMappings: {[index: string]: any} = {
    SumOfAggregate: MySqlSumOfAggregate,
    AverageAggregate: MySqlAverageAggregate,
    MinimumAggregate: MySqlMinimumAggregate,
    MaximumAggregate: MySqlMaximumAggregate,
    CountAggregate: MySqlCountAggregate,
    CountDistinctAggregate: MySqlCountDistinctAggregate,
  };

  private static connectionPools: {[index: string]: Pool} = {};

  constructor(connectionSettings: MySqlConnectionSettings) {
    super();
    this.connectionSettings = connectionSettings;
  }

  public static closeAllConnections() {
    for (const [conStr, pool] of Object.entries(MySqlStore.connectionPools)) {
      pool.end();
    }
  }

  public static getColumnSchema(column: Column): MySqlColumnSchema {
    return new MySqlStore.columnSchemaMappings[column.constructor.name](column);
  }

  public static getMappedFilter<T extends Filter>(filter: Filter): MySqlFilter<T> {
    return new MySqlStore.filterMappings[filter.constructor.name](filter);
  }

  public static getMappedAggregate<T extends Aggregate>(aggregate: Aggregate): MySqlAggregate<T> {
    return new MySqlStore.aggregateMappings[aggregate.constructor.name](aggregate);
  }

  async getConnection() : Promise<Connection> {
    const connectionSignature = JSON.stringify(this.connectionSettings);

    if (!MySqlStore.connectionPools[connectionSignature]) {
      MySqlStore.connectionPools[connectionSignature]
          = mysql.createPool({
            connectionLimit: 100,
            host: this.connectionSettings.host,
            user: this.connectionSettings.user,
            password: this.connectionSettings.password,
            database: this.connectionSettings.database,
            port: this.connectionSettings.port,
          });
    }

    const pool = MySqlStore.connectionPools[connectionSignature];

    // @ts-ignore
    return pool.getConnection();
  }

  private async openConnection() {
    if (!this.connection) {
      this.connection = await this.getConnection();
      await this.connection.execute('SET sql_mode=\'\'');
    }
  }

  async execute(sql: string, params?: any[]): Promise<[RowDataPacket[], FieldPacket[]]> {
    await this.openConnection();

    if (!this.connection) {
      /// TODO: Throw error that there is no connection!
      throw new Error();
    }    

    return this.connection.execute(sql, params);
  }

  async getSingleEntity<T extends Entity>(c: { new(): T }, id: string | number): Promise<T> {
    await this.openConnection();

    const result = await this.execute('SELECT * FROM tblTest');
    if (result) {
      const [rows, fields] = result;
    }

    return new c();
  }

  async storeEntity<T extends Entity>(entity: T, repository: Repository<T>): Promise<T> {
    await this.openConnection();

    const schemaColumns: any = [];
    const params: any = [];
    const columns = Column.getColumnsFromEntity(entity);

    Object.values(columns).forEach((column: Column) => {
      if (column.autoincrement) {
        return;
      }

      // @ts-ignore
      if (entity[column.targetProperty] !== undefined) {
        schemaColumns.push(`\`${column.name}\` = ?`);

        const mysqlColumn = MySqlStore.getColumnSchema(column);
        // @ts-ignore
        params.push(mysqlColumn.transformDataForStorage(entity[column.targetProperty]));
      }
    });

    let sql = '';

    if (!entity.id) {
      sql = `INSERT INTO \`${repository.name}\` SET ${schemaColumns.join(', ')}`;
    } else {
      const uniqueIdentiferColumn = repository.getUniqueIdentifier();
      if (uniqueIdentiferColumn) {
        sql = `UPDATE \`${repository.name}\` SET ${schemaColumns.join(', ')}
                 WHERE \`${uniqueIdentiferColumn.name}\` = ?`;

        params.push(entity[uniqueIdentiferColumn.targetProperty]);
      } else {
        throw new CantStoreEntityError('Entity has no uniqueIdentifier', entity);
      }
    }

    const result = await this.execute(sql, params);

    /// @ts-ignore
    if (result && result[0].insertId) {
      /// @ts-ignore
      entity.id = result[0].insertId;
    }

    return entity;
  }

  public static getSchemaSqlFragment(column: Column): string {
    let columnDefinition: string = `\`${column.name}\` `;

    const mysqlColumn = MySqlStore.getColumnSchema(column);

    columnDefinition += mysqlColumn.getSchemaSql();

    if (column.isUniqueIdentifier || column.defaultValue !== null) {
      columnDefinition += ' NOT NULL';
      if (column.defaultValue !== null) {
        columnDefinition += ` DEFAULT '${mysqlColumn.transformDataForStorage(column.defaultValue)}' `;
      }
    } else {
      columnDefinition += ' DEFAULT NULL';
    }

    if (column.autoincrement) {
      columnDefinition += ' auto_increment';
    }

    if (column.isUniqueIdentifier) {
      columnDefinition += ' PRIMARY KEY';
    }

    return columnDefinition;
  }

  public async checkAllSchemas() {
    const promises: any[] = [];
    this.getRepositories().forEach((repository) => {
      promises.push(this.checkSchema(repository));
    });

    await Promise.all(promises);
  }

  public async checkSchema(repository: Repository<Entity>) {
    let comparisonTableSchema;

    try {
      comparisonTableSchema = await this.parseSchema(repository.name);
    } catch (e) {
    }

    if (comparisonTableSchema) {
      await this.updateSchema(repository, comparisonTableSchema);
    } else {
      await this.createSchema(repository);
    }
  }

  private async updateSchema(repository: Repository<Entity>, existingSchema: ComparisonTableSchema) {
    const columns = Object.values(repository.getStorageColumns());
    const alters: string[] = [];
    const found: string[] = [];
    for (const name in existingSchema.columns) {
      const existingColumnSchema = existingSchema.columns[name];
      const foundColumns = columns.filter(column => column.name === name);

      if (foundColumns.length > 0) {
        let newSchema = MySqlStore.getSchemaSqlFragment(foundColumns[0]);
        newSchema = newSchema.replace(' PRIMARY KEY', '').trim();

        if (existingColumnSchema.toLowerCase() !== newSchema.toLowerCase()) {
          alters.push(`CHANGE COLUMN \`${name}\` ${newSchema}`);
        }

        found.push(name);
      }
    }

    const missingColumns = columns.filter(column => found.indexOf(column.name) === -1);
    for (const column of missingColumns) {
      const newSchema = MySqlStore.getSchemaSqlFragment(column);
      alters.push(`ADD COLUMN ${newSchema}`);
    }

    if (alters.length > 0) {
      let sql = alters.join(', ');
      sql = `ALTER TABLE \`${repository.name}\` ${sql}`;

      await this.execute(sql);
    }
  }

  private async createSchema(repository: Repository<Entity>) {
    let createSql = `CREATE TABLE \`${repository.name}\` (`;

    const columnSqlFragments: string[] = [];
    const columns = repository.getStorageColumns();

    Object.values(columns).map((column: Column) => {
      columnSqlFragments.push(MySqlStore.getSchemaSqlFragment(column));
    });

    createSql += `${columnSqlFragments.join(', ')})`;

    await this.execute(createSql);
  }

  public async parseSchema(tableName: string): Promise<ComparisonTableSchema> {
    const schema = new ComparisonTableSchema();
    schema.name = tableName;
    schema.columns = {};

    const result = await this.execute(`SHOW CREATE TABLE ${tableName}`);

    if (result) {
      const [rows, fields] = result;

      const createString = rows[0]['Create Table'];

      let lines = createString.split('\n');

      // First and last lines aren't needed
      lines = lines.slice(1, -1);

      const indexKeywords = ['PRIMARY', 'KEY', 'UNIQUE', 'FULLTEXT'];

      for (let line of lines) {
        line = line.trim();

        // If the line starts with a back tick we have a column
        if (line[0] === '`') {
          const matches = line.replace(/,$/, '').match(/`([^`]+)`/);
          const name = matches[1];

          schema.columns[name] = line.replace(/,$/, '');
        } else {
          const words = line.split(' ');
          /*
                  if (indexKeywords.indexOf(words[0]) > -1) {
                    index = preg_replace('/\s+/', ' ', rtrim(trim($line), ","));
                    index = preg_replace('/^UNIQUE KEY/', 'UNIQUE', $index);
                    $comparisonSchema->indexes[] = $index;

          */
        }
      }      
    }

    return schema;
  }

  public getSqlStatement<T extends Entity>(
      repository: Repository<T>,
      filter: Filter,
      sorts: Sort[],
      groups: string[],
      intersections: Join[],
      aggregates: Aggregate[]): [SqlStatement, {[index:string]: Column}] {

    const entity = repository.newEntity();
    const columns = Column.getColumnsFromEntity(entity);
    const statement = new SqlStatement();
    statement.tableName = repository.name;

    // Add native columns to the outer collection
    Object.values(columns).map((column: Column) => {
      // @ts-ignore
      const sqlColumn = new SqlColumn();
      sqlColumn.columnName = column.name;
      sqlColumn.alias = column.targetProperty;
      sqlColumn.statementAlias = statement.alias;
      statement.columns.push(sqlColumn);
    });

    intersections.map((intersection) => {
      const [subStatement, subColumns] = this.getSqlStatement<T>(
          intersection.collection.getRepository(),
          intersection.collection,
          intersection.collection.sorts,
          intersection.collection.groups,
          intersection.collection.intersections,
          intersection.collection.aggregates,
      );

      const sqlJoin = new SqlJoin(subStatement);
      sqlJoin.parentStatementAlias = statement.alias;
      sqlJoin.joinType = intersection.joinType;
      sqlJoin.onSource = intersection.sourceColumn;
      sqlJoin.onDestination = intersection.targetColumn;
      sqlJoin.pullUps = intersection.pullUps;

      for (const [column, alias] of Object.entries(intersection.pullUps)) {
        columns[alias] = subColumns[column];
      }

      statement.joins.push(sqlJoin);
    });

    Object.values(aggregates).map((aggregate: Aggregate) => {
      // @ts-ignore
      const mysqlAggregate = MySqlStore.getMappedAggregate(aggregate);
      statement.aggregates.push(mysqlAggregate.getAggregateColumn());

      columns[aggregate.alias] = columns[aggregate.columnName];
    });

    // Add columns pulled up out of any intersections
    statement.joins.map((join: SqlJoin) => {
      for (const [key, value] of Object.entries(join.pullUps)) {
        const sqlColumn = new SqlColumn();
        sqlColumn.columnName = key;
        sqlColumn.alias = value;
        sqlColumn.statementAlias = join.statement.alias;
        statement.columns.push(sqlColumn);
      }
    });

    if (filter) {
      const mysqlFilter = MySqlStore.getMappedFilter(filter);
      statement.whereExpression = mysqlFilter.getWhereExpression();
    }

    const sortExpressions = sorts.map((sort) => {
      const sortExpression = new SqlOrderBy();
      sortExpression.column = sort.column;
      sortExpression.direction = sort.direction;
      return sortExpression;
    });

    statement.sorts.push(...sortExpressions);
/*
    if (aggregates.length > 0 && groups.length === 0) {
      // If we're aggregating, then we need to group by something - nothing
      // else makes sense and MySql will complain. In this case the
      // unique identifier is a good bet.
      const uniqueColumn = Object.values(columns).find(column => column.isUniqueIdentifier);

      if (uniqueColumn){
        groups.push(uniqueColumn.name);
      }
    }
*/
    const groupExpressions = groups.map((group) => {
      const groupExpression = new SqlGroup();
      groupExpression.column = group;
      return groupExpression;
    });

    statement.groups.push(...groupExpressions);

    return [statement, columns];
  }

  async fetchItems<T extends Entity>(
      repository: Repository<T>,
      filter: Filter,
      sorts: Sort[],
      groups: string[],
      intersections: Join[],
      aggregates: Aggregate[]): Promise<ResultSet<T>> {

    const [statement, selectedColumns] = this.getSqlStatement(
        repository,
        filter,
        sorts,
        groups,
        intersections,
        aggregates);

    const [sql, params] = statement.getSql();

    console.log(sql);
    const result = await this.execute(sql, params);

    if (result) {
      const [rows, fields ] = result;
      return new MySqlResultSet<T>(rows, repository, selectedColumns);
    } else {
      /// TODO - something better here
      throw new Error();
    }
  }

  async deleteEntity<T extends Entity>(entity: T, repository: Repository<T>): Promise<void> {

    const uniqueIdentifierColumn = repository.getUniqueIdentifier();

    if (uniqueIdentifierColumn) {
      await this.execute(
          `DELETE FROM \`${repository.name}\` WHERE \`${uniqueIdentifierColumn.name}\` = ?`,
          [entity[uniqueIdentifierColumn.targetProperty]]);
    }
  }
}
