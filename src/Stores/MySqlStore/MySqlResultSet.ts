import { ResultSet } from '../ResultSet';
import { Entity } from '../../Entities/Entity';
import { Repository } from '../../Repositories/Repository';
import { Column } from '../../Schema/Columns/Column';
import { MySqlStore } from './MySqlStore';

export class MySqlResultSet<T extends Entity> extends ResultSet<T>{
  private rawData: {}[];
  private repository: Repository<T>;
  private columns: {[index: string]: Column};

  constructor(rawData: {}[], repository: Repository<T>, selectedColumns: {[index: string]: Column}) {
    super();
    this.rawData = rawData;
    this.repository = repository;
    this.columns = selectedColumns;
    this.totalLength = rawData.length;
  }

  protected makeEntityForData(rawData: {[index: string]: any}) {
    const entity = this.repository.newEntity();

    for (const [key, value] of Object.entries(rawData)) {
      if (this.columns[key]) {
        const column = this.columns[key];
        const mysqlColumn = MySqlStore.getColumnSchema(column);

        /// TODO: This is a cheat - we're trying to handle both native columns that are
        /// mapped using the decorators on an entity (i.e. targetProperty) but also those
        /// that are aliases by a developer when using pull ups or aggregates.
        ///
        /// The columns collection needs improved to include the entity property name
        /// to take away the guess work.
        if (column.name === key) {
          // @ts-ignore
          entity[column.targetProperty] = mysqlColumn.trasformDataFromStorage(rawData[key]);
        } else {
          // @ts-ignore
          entity[key] = mysqlColumn.trasformDataFromStorage(rawData[key]);
        }
      } else {
        // Only a fall back in case the result set doesn't have schema information for a given
        // column name. It should however - as that can be passed to our constructor in the
        // selectedColumns property
        // @ts-ignore
        entity[key] = value;
      }
    }

    return entity;
  }

  public [Symbol.iterator](): Iterator<T> {
    let pointer = -1;
    const items = this.rawData;
    const makeEntityForData = this.makeEntityForData;

    return {
      next(): IteratorResult<T> {
        if (pointer < items.length) {
          pointer = pointer + 1;
          return {
            done: false,
            value: makeEntityForData(items[pointer]),
          };
        }
        return {
          done: true,
          /// @ts-ignore
          value: null,
        };
      },
    };
  }

  item(index: number): T {
    return this.makeEntityForData(this.rawData[index]);
  }
}
