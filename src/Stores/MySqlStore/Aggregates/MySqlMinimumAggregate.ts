import { MySqlAggregate } from './MySqlAggregate';
import { SqlAggregateAsColumn } from '../SqlTokens/SqlAggregateAsColumn';
import { MinimumAggregate } from '../../../Collections/Aggregates/MinimumAggregate';

export class MySqlMinimumAggregate extends MySqlAggregate<MinimumAggregate> {
  getAggregateColumn(): SqlAggregateAsColumn {
    return new SqlAggregateAsColumn(
        `MIN(\`${this.aggregate.columnName}\`)`,
        this.aggregate.alias,
    );
  }
}
