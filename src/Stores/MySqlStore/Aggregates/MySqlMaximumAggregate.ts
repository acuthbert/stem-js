import { MySqlAggregate } from './MySqlAggregate';
import { SqlAggregateAsColumn } from '../SqlTokens/SqlAggregateAsColumn';
import { MaximumAggregate } from '../../../Collections/Aggregates/MaximumAggregate';

export class MySqlMaximumAggregate extends MySqlAggregate<MaximumAggregate> {
  getAggregateColumn(): SqlAggregateAsColumn {
    return new SqlAggregateAsColumn(
        `MAX(\`${this.aggregate.columnName}\`)`,
        this.aggregate.alias,
    );
  }
}
