import { Aggregate } from '../../../Collections/Aggregates/Aggregate';
import {SqlAggregateAsColumn} from '../SqlTokens/SqlAggregateAsColumn';

export abstract class MySqlAggregate<T extends Aggregate> {
  protected aggregate: T;

  constructor(aggregate: T) {
    this.aggregate = aggregate;
  }

  abstract getAggregateColumn(): SqlAggregateAsColumn;
}
