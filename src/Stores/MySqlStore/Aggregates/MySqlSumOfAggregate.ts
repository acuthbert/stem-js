import {MySqlAggregate} from './MySqlAggregate';
import {SumOfAggregate} from '../../../Collections/Aggregates/SumOfAggregate';
import {SqlAggregateAsColumn} from '../SqlTokens/SqlAggregateAsColumn';

export class MySqlSumOfAggregate extends MySqlAggregate<SumOfAggregate> {
  getAggregateColumn(): SqlAggregateAsColumn {
    return new SqlAggregateAsColumn(
        `SUM(\`${this.aggregate.columnName}\`)`,
        this.aggregate.alias,
    );
  }
}
