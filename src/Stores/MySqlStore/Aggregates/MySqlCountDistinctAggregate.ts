import { MySqlAggregate } from './MySqlAggregate';
import { SqlAggregateAsColumn } from '../SqlTokens/SqlAggregateAsColumn';
import { MaximumAggregate } from '../../../Collections/Aggregates/MaximumAggregate';

export class MySqlCountDistinctAggregate extends MySqlAggregate<MaximumAggregate> {
  getAggregateColumn(): SqlAggregateAsColumn {
    return new SqlAggregateAsColumn(
        `COUNT(DISTINCT \`${this.aggregate.columnName}\`)`,
        this.aggregate.alias,
    );
  }
}
