import { MySqlAggregate } from './MySqlAggregate';
import { AverageAggregate } from '../../../Collections/Aggregates/AverageAggregate';
import { SqlAggregateAsColumn } from '../SqlTokens/SqlAggregateAsColumn';

export class MySqlAverageAggregate extends MySqlAggregate<AverageAggregate> {
  getAggregateColumn(): SqlAggregateAsColumn {
    return new SqlAggregateAsColumn(
            `AVG(\`${this.aggregate.columnName}\`)`,
            this.aggregate.alias);
  }
}
