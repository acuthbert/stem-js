import { MySqlAggregate } from './MySqlAggregate';
import { SqlAggregateAsColumn } from '../SqlTokens/SqlAggregateAsColumn';
import { MaximumAggregate } from '../../../Collections/Aggregates/MaximumAggregate';

export class MySqlCountAggregate extends MySqlAggregate<MaximumAggregate> {
  getAggregateColumn(): SqlAggregateAsColumn {
    return new SqlAggregateAsColumn(
        `COUNT(\`${this.aggregate.columnName}\`)`,
        this.aggregate.alias,
    );
  }
}
