import { Column } from '../../Schema/Columns/Column';

export class ComparisonTableSchema {
  public name: string = '';
  public columns: {[key: string]: string} = {};
}
