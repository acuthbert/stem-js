export declare class ComparisonTableSchema {
    name: string;
    columns: {
        [key: string]: string;
    };
}
