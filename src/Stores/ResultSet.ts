import { Entity } from '../Entities/Entity';

export abstract class ResultSet<T extends Entity> implements Iterable<T> {
  /**
   * Some collections are ranged so items will not contain all possible items.
   * Most stores can return the count of the full data set at the same time to allow
   * poging sizes to be calculated.
   *
   * @type {number}
   */
  public totalLength: number = 0;

  public abstract [Symbol.iterator](): Iterator<T>;

  public abstract item(index: number): T;
}
