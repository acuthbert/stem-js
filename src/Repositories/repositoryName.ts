export function repositoryName(repositoryName: string) {
  return function <T extends {new(...args:any[]):{}}>(constructor:T) {
    return class extends constructor {
      name = repositoryName;
    };
  };
}
