import { Entity } from '../Entities/Entity';
import { Column } from '../Schema/Columns/Column';
import { Collection } from '../Collections/Collection';
import { Filter } from '../Collections/Filters/Filter';
import { Store } from '../Stores/Store';
import { NoStoreError } from './NoStoreError';

export abstract class Repository<T extends Entity> {
  name: string = '';

  /**
   * Instantiates a new entitty
   */
  public newEntity(): T {
    const type = this.getEntityType();
    return new type();
  }

  protected backingStore?: Store;

  /**
   * Returns the type of entity the repository should instantiate
   *
   * Although we are using TypeScript generics, as JS does not support
   * generics, T is only a type hint and as such is not present in
   * the compiled JS
   */
  public abstract getEntityType(): any;

  /**
   * Returns an array of all column types required to store the entity.
   */
  public getStorageColumns(): {[index:string]: Column} {
    return Column.getColumnsFromEntity(this.newEntity());
  }

  public getUniqueIdentifier(): Column|undefined {
    return Object.values(this.getStorageColumns()).find(item => item.isUniqueIdentifier);
  }

  public setStore(store: Store) {
    this.backingStore = store;
  }

  public getStore(): Store {
    if (!this.backingStore) {
      throw new NoStoreError();
    }

    return this.backingStore;
  }

  public async store(entity: T): Promise<T> {
    await this.getStore().storeEntity(entity, this);

    return entity;
  }

  public async delete(entity: T): Promise<void> {
    await this.getStore().deleteEntity(entity, this);
  }

  public where(property: string|Filter): Collection<T> {
    const collection = this.all();
    collection.where(property);
    return collection;
  }

  public all(): Collection<T> {
    return new Collection<T>(this);
  }

  public async one(dictionaryOrFilter: {[index:string]: any}|Filter): Promise<T> {
    const collection = this.all();

    if (dictionaryOrFilter instanceof Filter) {
      collection.where(dictionaryOrFilter);
    } else {
      for (const key of Object.keys(dictionaryOrFilter)) {
        collection.where(key).equals(dictionaryOrFilter[key]);
      }
    }

    await collection.fetch();

    return collection.item(0);
  }
}
