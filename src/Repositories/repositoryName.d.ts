export declare function repositoryName(repositoryName: string): <T extends new (...args: any[]) => {}>(constructor: T) => {
    new (...args: any[]): {
        name: string;
    };
} & T;
