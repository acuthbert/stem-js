export enum ColumnType {
    Boolean,
    String,
    Integer,
    Decimal,
    Date,
    DateTime,
}
