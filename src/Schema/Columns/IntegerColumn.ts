import { Column } from './Column';
import { ColumnType } from './ColumnType';

export class IntegerColumn extends Column {
  signed: boolean = true;

  constructor() {
    super();
    this.type = ColumnType.Integer;
  }
}
