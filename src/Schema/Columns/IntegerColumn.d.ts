import { Column } from './Column';
export declare class IntegerColumn extends Column {
    signed: boolean;
}
