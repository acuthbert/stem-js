import { Column } from './Column';

export class BooleanColumn extends Column {
  constructor() {
    super(false);
  }
}
