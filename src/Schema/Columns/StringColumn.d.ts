import { Column } from './Column';
export declare class StringColumn extends Column {
    length: number;
    constructor(length: number, defaultValue: any);
}
