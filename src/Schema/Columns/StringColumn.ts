import { Column } from './Column';
import { ColumnType } from './ColumnType';

export class StringColumn extends Column {
  length: number = 0;

  constructor(length :number, defaultValue: any) {
    super(defaultValue);
    this.type = ColumnType.String;
    this.length = length;
  }
}
