import { Entity } from '../../Entities/Entity';
export declare class Column {
    targetProperty: string;
    name: string;
    defaultValue: any;
    isUniqueIdentifier: boolean;
    length: number | null;
    type: any;
    autoincrement: boolean;
    constructor(defaultValue?: any);
    /**
     * Returns an array of all column type decorators on an entity.
     */
    static getColumnsFromEntity(entity: Entity): Column[];
}
