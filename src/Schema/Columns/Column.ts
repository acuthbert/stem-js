import { Entity } from '../../Entities/Entity';
import { getColumns } from '../../Entities/Decorators/column';
import { ColumnType } from './ColumnType';

export class Column {
  targetProperty: string = '';
  name: string = '';
  defaultValue: any;
  isUniqueIdentifier = false;
  length: number | null = null;
  type?: ColumnType;
  autoincrement = false;
  [index: string]: any;

  constructor(defaultValue: any = null) {
    this.defaultValue = defaultValue;    
  }

  /**
   * Returns an array of all column type decorators on an entity.
   */
  public static getColumnsFromEntity(entity: Entity): {[index: string]: Column} {
    return { ...getColumns(entity) };
  }
}
