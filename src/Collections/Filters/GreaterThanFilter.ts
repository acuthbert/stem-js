import { ColumnAndValueFilter } from './ColumnAndValueFilter';

export class GreaterThanFilter extends ColumnAndValueFilter {
  public inclusive: boolean = true;

  public constructor(column: string, value: string, inclusive: boolean = true) {
    super(column, value);

    this.inclusive = inclusive;
  }
}
