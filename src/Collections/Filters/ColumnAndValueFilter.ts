import { Filter } from './Filter';

export class ColumnAndValueFilter extends Filter {
  public column: string;
  public value: string;

  public constructor(column: string, value: string){
    super();
    this.column = column;
    this.value = value;
  }
}
