import { Filter } from './Filter';
import { EqualsFilter } from './EqualsFilter';
import { BooleanType } from '../../Shared/BooleanType';
import { GreaterThanFilter } from './GreaterThanFilter';
import { LessThanFilter } from './LessThanFilter';
import { AndGroupFilter } from './AndGroupFilter';
import { StartsWithFilter } from './StartsWithFilter';
import { EndsWithFilter } from './EndsWithFilter';
import {ContainsFilter} from './ContainsFilter';

export abstract class GroupFilter extends Filter {

  protected booleanType: BooleanType = BooleanType.And;
  protected filters: Filter[] = [];

  public equals(value: string): this {
    this.filters.push(new EqualsFilter(this.expressionBuilderPropertyName, value));
    return this;
  }

  public contains(phrase: string): this {
    this.filters.push(new ContainsFilter(this.expressionBuilderPropertyName, phrase));
    return this;
  }

  public endsWith(phrase: string): this {
    this.filters.push(new EndsWithFilter(this.expressionBuilderPropertyName, phrase));
    return this;
  }

  public startsWith(phrase: string): this {
    this.filters.push(new StartsWithFilter(this.expressionBuilderPropertyName, phrase));
    return this;
  }

  public between(from: any, to: any): this {
    const group = new AndGroupFilter();
    group.where(this.expressionBuilderPropertyName)
        .greaterThanOrEqualTo(from)
        .lessThanOrEqualTo(to);

    this.filters.push(group);
    return this;
  }

  public lessThanOrEqualTo(value: string): this {
    this.filters.push(new LessThanFilter(this.expressionBuilderPropertyName, value, true));
    return this;
  }

  public lessThan(value: string): this {
    this.filters.push(new LessThanFilter(this.expressionBuilderPropertyName, value, false));
    return this;
  }

  public greaterThanOrEqualTo(value: string): this {
    this.filters.push(new GreaterThanFilter(this.expressionBuilderPropertyName, value, true));
    return this;
  }

  public greaterThan(value: string): this {
    this.filters.push(new GreaterThanFilter(this.expressionBuilderPropertyName, value, false));
    return this;
  }

  public and(property: string|GroupFilter): this {
    if (this.booleanType !== BooleanType.And) {
      throw new Error('Only AND groups can use and()');
    }

    return this.where(property);
  }

  public or(property: string|GroupFilter): this {
    if (this.booleanType !== BooleanType.Or) {
      throw new Error('Only OR groups can use or()');
    }

    return this.where(property);
  }

  protected expressionBuilderPropertyName: string = '';

  public where(property: string|Filter): this {
    if (typeof property === 'string') {
      this.expressionBuilderPropertyName = property.toString();
    } else {
      this.filters.push(property);
    }

    return this;
  }

  public getFilters(): Filter[] {
    return this.filters;
  }
}
