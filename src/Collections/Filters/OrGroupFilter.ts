import { GroupFilter } from './GroupFilter';
import { BooleanType } from '../../Shared/BooleanType';

export class OrGroupFilter extends GroupFilter {
  public constructor() {
    super();
    this.booleanType = BooleanType.Or;
  }
}
