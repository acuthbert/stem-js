import { Repository } from '../Repositories/Repository';
import { Entity } from '../Entities/Entity';
import { AndGroupFilter } from './Filters/AndGroupFilter';
import { ResultSet } from '../Stores/ResultSet';
import { CollectionNotFetchedError } from './Errors/CollectionNotFetchedError';
import { Join } from './Joins/Join';
import { Sort, SortDirection } from './Sort';
import { JoinType } from './Joins/JoinType';
import { Aggregate } from './Aggregates/Aggregate';

export class Collection<T extends Entity>  extends AndGroupFilter implements IterableIterator<T> {

  public items?: ResultSet<T>;

  private length: number = 0;
  private readonly repos: Repository<T>;
  public intersections: Join[] = [];
  public sorts: Sort[] = [];
  public groups: string[] = [];
  public aggregates: Aggregate[] = [];

  public constructor(repos: Repository<T>) {
    super();
    this.repos = repos;
  }

  public getRepository(): Repository<T> {
    return this.repos;
  }

  public groupBy(column: string): Collection<T> {
    this.groups.push(column);
    return this;
  }

  public joinWith(
      collection: Collection<any>,
      sourceColumn: string,
      targetColumn: string,
      pullUps = {},
      ): Collection<T> {

    const intersection = new Join(
        collection,
        sourceColumn,
        targetColumn,
        JoinType.Join,
        pullUps);

    this.intersections.push(intersection);

    return this;
  }

  public intersectWith(
      collection: Collection<any>,
      sourceColumn: string,
      targetColumn: string,
      pullUps = {},
      ): Collection<T> {

    const intersection = new Join(
        collection,
        sourceColumn,
        targetColumn,
        JoinType.Intersection,
        pullUps);

    this.intersections.push(intersection);

    return this;
  }

  public item(index: number): T {
    if (!this.fetched) {
      throw new CollectionNotFetchedError();
    }

    if (this.items) {
      return this.items.item(index);
    }

    throw new CollectionNotFetchedError();
  }

  public sortBy(column:string, direction: SortDirection = SortDirection.Ascending): Collection<T> {
    this.sorts.push(new Sort(column, direction));
    return this;
  }

  public addAggregate(aggregate: Aggregate, ...aggregates: Aggregate[]): Collection<T> {
    this.aggregates.push(aggregate);

    if (aggregates) {
      this.aggregates.push(...aggregates);
    }

    return this;
  }

  protected fetched = false;

  public invalidate() {
    this.fetched = false;
    this.items = undefined;
    this.iterator = undefined;
  }

  public async fetch() {
    if (this.fetched) {
      return;
    }

    const store = this.repos.getStore();

    const results = await store.fetchItems(
      this.repos,
      this,
      this.sorts,
      this.groups,
      this.intersections,
      this.aggregates,
    );

    this.fetched = true;
    this.items = results;
  }

  public count() {
    if (!this.fetched) {
      throw new CollectionNotFetchedError();
    }

    if (this.items) {
      return this.items.totalLength;
    }

    throw new CollectionNotFetchedError();
  }

  public [Symbol.iterator](): IterableIterator<T> {
    return this;
  }

  iterator?: Iterator<T>;

  next(value?: any): IteratorResult<T> {
    if (!this.fetched) {
      throw new CollectionNotFetchedError();
    }

    if (!this.items) {
      throw new CollectionNotFetchedError();
    }

    if (!this.iterator) {
      this.iterator = this.items[Symbol.iterator]();
    }

    if (this.iterator) {
      const result = this.iterator.next();
      return result;
    }

    throw new CollectionNotFetchedError();
  }
}
