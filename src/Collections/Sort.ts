export enum SortDirection {
  Ascending,
  Descending,
}

export class Sort {
  constructor(
    public column = '',
    public direction = SortDirection.Ascending,
  ) {}
}
