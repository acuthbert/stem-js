import { Collection } from '../Collection';
import { JoinType } from './JoinType';

export class Join {
  constructor(
      public collection: Collection<any>,
      public sourceColumn: string,
      public targetColumn: string,
      public joinType: JoinType = JoinType.Intersection,
      public pullUps: {[index:string]: string},
  ) {
  }
}
