import { Aggregate } from './Aggregate';
import { Entity } from '../../Entities/Entity';

export class MaximumAggregate extends Aggregate {

  accumulate(lastValue: any, item: Entity): any {
    return Math.max((lastValue ? lastValue : 0), item[this.columnName]);
  }

  aggregate(accumulatedValue: any, count: number, items: Entity[]): any {
    return accumulatedValue;
  }
}
