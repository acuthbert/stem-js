import { Aggregate } from './Aggregate';
import { Entity } from '../../Entities/Entity';

export class CountDistinctAggregate extends Aggregate {

  accumulate(lastValue: any, item: Entity): any {
    return (lastValue ? lastValue.add(item[this.columnName]) : new Set([item[this.columnName]]));
  }

  aggregate(accumulatedValue: any, count: number, items: Entity[]): any {
    return accumulatedValue.size;
  }
}
