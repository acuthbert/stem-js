import { Aggregate } from './Aggregate';
import { Entity } from '../../Entities/Entity';

export class MinimumAggregate extends Aggregate {

  accumulate(lastValue: any, item: Entity): any {
    return lastValue ? Math.min(lastValue, item[this.columnName]) : item[this.columnName];
  }

  aggregate(accumulatedValue: any, count: number, items: Entity[]): any {
    return accumulatedValue;
  }
}
