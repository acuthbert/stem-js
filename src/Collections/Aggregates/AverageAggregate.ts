import { Entity } from '../../Entities/Entity';
import { SumOfAggregate } from './SumOfAggregate';

export class AverageAggregate extends SumOfAggregate {
  aggregate(accumulatedValue: any, count: number, items: Entity[]): any {
    return accumulatedValue / count;
  }
}
