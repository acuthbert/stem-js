import { Aggregate } from './Aggregate';
import { Entity } from '../../Entities/Entity';

export class CountAggregate extends Aggregate {

  accumulate(lastValue: any, item: Entity): any {
    return (lastValue ? lastValue : 0) + (item[this.columnName] ? 1 : 0);
  }

  aggregate(accumulatedValue: any, count: number, items: Entity[]): any {
    return accumulatedValue;
  }
}
