import { Entity } from '../../Entities/Entity';

export abstract class Aggregate {
  constructor(public columnName: string,
              public alias: string) {

  }

  abstract accumulate(lastValue: any, item: Entity): any;
  abstract aggregate(accumulatedValue: any, count: number, items: Entity[]): any;

  public getAggregatedValues(rows: Entity[], groups: string[]): {[index: string]: any} {
    const results: {[index: string]: any} = {};
    const counts: {[index: string]: number} = {};

    rows.forEach((row) => {
      const groupKey = Aggregate.getGroupKey(row, groups);
      if (!results[groupKey]) {
        results[groupKey] = null;
        counts[groupKey] = 0;
      }

      results[groupKey] = this.accumulate(results[groupKey], row);
      counts[groupKey] = counts[groupKey] + 1;
    });

    for (const [key, value] of Object.entries(results)) {
      results[key] = this.aggregate(value, counts[key], rows);
    }

    return results;
  }

  public static getGroupKey(row: Entity, groups: string[]) {
    return groups.reduce((str: string, group: string) => {
      return str + row[group];
    }, '');
  }
}
