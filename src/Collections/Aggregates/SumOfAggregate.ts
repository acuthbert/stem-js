import { Aggregate } from './Aggregate';
import { Entity } from '../../Entities/Entity';

export class SumOfAggregate extends Aggregate {

  accumulate(lastValue: any, item: Entity): any {
    const currentValue = (lastValue === null) ? 0 : lastValue;
    return currentValue + item[this.columnName];
  }

  aggregate(accumulatedValue: any, count: number, items: Entity[]): any {
    return accumulatedValue;
  }
}
