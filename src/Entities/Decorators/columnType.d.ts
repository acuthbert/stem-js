import 'reflect-metadata';
import { Column } from '../../Schema/Columns/Column';
export declare function getOrCeateColumnDefinition(target: any, property: string | symbol): Column;
export declare function columnType(columnType: string): PropertyDecorator;
export declare function getColumns(target: any): Column[];
export declare function getColumn(target: any, propertyKey: string): any;
