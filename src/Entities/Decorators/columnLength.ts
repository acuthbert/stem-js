import { getOrCeateColumnDefinition } from './columnType';

export function columnLength(length: number):PropertyDecorator {
  return function (target: any, property: string|symbol): void {

    const column = getOrCeateColumnDefinition(target, property);
    column.length = length;
  };
}
