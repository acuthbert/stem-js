import 'reflect-metadata';
export declare function isUniqueIdentifier(): PropertyDecorator;
export declare function getIsUniqueIdentifier(target: any, propertyKey: string): boolean;
