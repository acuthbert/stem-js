import 'reflect-metadata';
import { Column } from '../../Schema/Columns/Column';
import { ColumnType } from '../../Schema/Columns/ColumnType';

export function getOrCeateColumnDefinition(target: any, property: string|symbol): Column {

  if (Reflect.hasMetadata('stemColumn', target, property)) {
    return Reflect.getMetadata('stemColumn', target, property);
  }

  const column = new Column();
  column.targetProperty = property.toString();
  column.name = property.toString();

  let stemColumns = Reflect.getOwnMetadata('stemColumns', target);
  if (!stemColumns) {
    // merge with inherited fields, if available.
    stemColumns = Reflect.hasMetadata('stemColumns', target)
      ? Reflect.getMetadata('stemColumns', target).slice(0)
      : [];

    // define own fields on the target
    Reflect.defineMetadata('stemColumns', stemColumns, target);
  }

  stemColumns.push(column);

  Reflect.defineMetadata('stemColumn', column, target, property);

  return column;
}

export function columnType(columnType: ColumnType):PropertyDecorator {
  return function (target: Object, property: string|symbol): void {

    const column = getOrCeateColumnDefinition(target, property);
    column.type = columnType;
  };
}