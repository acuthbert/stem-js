import 'reflect-metadata';

export function autoincrement(): PropertyDecorator {
  return function (target: any, property: string|symbol): void {
    const columnType = Reflect.getMetadata('stemColumn', target, property);
    
    if (!columnType) {
      throw new Error('No @column() defined. Define this first.');
    }

    columnType.autoincrement = true;

    Reflect.defineMetadata('autoincrement', true, target, property);
  };
}

export function getAutoIncrement(target: any, propertyKey: string): boolean {
  return Reflect.getMetadata('autoincrement', target, propertyKey);
}
