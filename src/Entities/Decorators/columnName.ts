import { getOrCeateColumnDefinition } from './columnType';

export function columnName(name: string):PropertyDecorator {
  return function (target: any, property: string|symbol): void {

    const column = getOrCeateColumnDefinition(target, property);
    column.name = name;
  };
}
