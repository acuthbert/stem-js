import { getOrCeateColumnDefinition } from './columnType';

export function defaultValue(defaultValue: string):PropertyDecorator {
  return function (target: any, property: string|symbol): void {

    const column = getOrCeateColumnDefinition(target, property);
    column.defaultValue = defaultValue;
  };
}
