import 'reflect-metadata';
import { Column } from '../../Schema/Columns/Column';

export function column(column: Column):PropertyDecorator {
  return function (target: Object, property: string|symbol): void {

    let stemColumns = Reflect.getOwnMetadata('stemColumns', target);
    if (!stemColumns) {
          // merge with inherited fields, if available.
      stemColumns = Reflect.hasMetadata('stemColumns', target)
              ? Reflect.getMetadata('stemColumns', target).slice(0)
              : {};

          // define own fields on the target
      Reflect.defineMetadata('stemColumns', stemColumns, target);
    }

    column.name = property.toString();
    column.targetProperty = property.toString();
    stemColumns[column.name] = column;
    Reflect.defineMetadata('stemColumn', column, target, property);
  };
}

export function getColumns(target: any): {[index: string]: Column} {
  return Reflect.hasMetadata('stemColumns', target)
    ? Reflect.getMetadata('stemColumns', target)
    : [];
}

export function getColumn(target: any, propertyKey: string) {
  return Reflect.getMetadata('stemColumn', target, propertyKey);
}
