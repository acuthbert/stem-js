import 'reflect-metadata';
export declare function autoincrement(): PropertyDecorator;
export declare function getAutoIncrement(target: any, propertyKey: string): boolean;
