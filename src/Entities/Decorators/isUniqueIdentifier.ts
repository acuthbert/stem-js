import 'reflect-metadata';

export function isUniqueIdentifier(): PropertyDecorator {
  return function (target: any, property: string|symbol): void {
    const columnType = Reflect.getMetadata('stemColumn', target, property);

    if (!columnType) {
      throw new Error('No @column() defined. Define this first.');
    }

    columnType.isUniqueIdentifier = true;

    Reflect.defineMetadata('isUniqueIdentifer', true, target, property);
  };
}

export function getIsUniqueIdentifier(target: any, propertyKey: string): boolean {
  return Reflect.getMetadata('isUniqueIdentifer', target, propertyKey);
}
