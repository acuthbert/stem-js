export declare class Singleton {
    private static instances;
    protected constructor();
    static getInstance(type: any, generateInstanceCallback: Function): any;
}
