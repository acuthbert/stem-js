export class Singleton {
  private static instances: {[key: string]: any} = {};

  protected constructor() {

  }

  public static getInstance(type: any, generateInstanceCallback: Function) {
    if (!this.instances[type.toString()]) {
      this.instances[type.toString()] = generateInstanceCallback();
    }

    return this.instances[type.toString()];
  }
}
