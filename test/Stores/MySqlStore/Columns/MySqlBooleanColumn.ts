import { expect } from 'chai';
import 'mocha';
import { column } from '../../../../src/Entities/Decorators/column';
import { repositoryName } from '../../../../src/Repositories/repositoryName';
import { Repository } from '../../../../src/Repositories/Repository';
import { IntegerColumn } from '../../../../src/Schema/Columns/IntegerColumn';
import { Entity } from '../../../../src/Entities/Entity';
import { columnName } from '../../../../src/Entities/Decorators/columnName';
import { MySqlStore } from '../../../../src/Stores/MySqlStore/MySqlStore';
import { autoincrement } from '../../../../src/Entities/Decorators/autoincrement';
import { isUniqueIdentifier } from '../../../../src/Entities/Decorators/isUniqueIdentifier';
import { BooleanColumn } from '../../../../src/Schema/Columns/BooleanColumn';
import { MySqlConnectionSettings } from '../../../../src/Stores/MySqlStore/MySqlConnectionSettings';

class TestEntity extends Entity {
  @isUniqueIdentifier()
  @autoincrement()
  @columnName('ID')
  @column(new IntegerColumn())
  id: number = 0;

  @column(new BooleanColumn())
  active: boolean = false;
}

@repositoryName('tblTest')
class TestRepository extends Repository<TestEntity> {
  public getEntityType(): any {
    return TestEntity;
  }
}

const connection = new MySqlConnectionSettings();
connection.host = 'localhost';
connection.password = 'unit-testing';
connection.database = 'unit-testing';
connection.user = 'unit-testing';
describe('MySqlBooleanColumn', () => {
  it('Can transform data', async () => {
    const store = new MySqlStore(connection);
    await store.execute('DROP TABLE IF EXISTS tblTest');

    const repos = new TestRepository();

    await store.checkSchema(repos);

    const entity = new TestEntity();
    entity.active = true;
    repos.store(entity);

    const fetched = await repos.one({ id: 1 });
    expect(fetched.active).to.be.true;
  });
});
