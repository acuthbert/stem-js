import { expect } from 'chai';
import 'mocha';
import { Repository } from '../../../src/Repositories/Repository';
import { MySqlStore } from '../../../src/Stores/MySqlStore/MySqlStore';
import { repositoryName } from '../../../src/Repositories/repositoryName';
import {
  TestContactEntity,
  TestContactsRepository,
  TestOrderEntity,
  TestOrdersRepository,
  testStore,
} from '../Store';
import { MySqlConnectionSettings } from '../../../src/Stores/MySqlStore/MySqlConnectionSettings';

@repositoryName('tblTest')
class TestRepository extends Repository<TestContactEntity> {
  public getEntityType(): any {
    return TestContactEntity;
  }
}

process.on('warning', function (e) {
  console.log(e.stack);
});

const connectionSettings = new MySqlConnectionSettings();
connectionSettings.host = 'localhost';
connectionSettings.password = 'unit-testing';
connectionSettings.database = 'unit-testing';
connectionSettings.user = 'unit-testing';

describe('MySqlStore', async () => {

  it('Can create schemas', async () => {
    const store = new MySqlStore(connectionSettings);

    await store.execute('DROP TABLE IF EXISTS tblTest');

    const repos = new TestRepository();

    await store.checkSchema(repos);

    const [rows, fields] = await store.execute('SELECT * FROM tblTest');

    expect(rows).length(0);
    expect(fields).length(4);

    const ordersRepos = new TestOrdersRepository();
    await store.checkSchema(ordersRepos);
  });

  it('Can parse existing schema', async () => {
    const store = new MySqlStore(connectionSettings);
    await store.execute('DROP TABLE IF EXISTS tblSchemaTest');
    await store.execute(
      'CREATE TABLE tblSchemaTest' +
              '(ID INT auto_increment PRIMARY KEY,' +
              'name VARCHAR(3) not null default \'abc\'' +
          ')');

    const schema = await store.parseSchema('tblSchemaTest');

    expect(schema.name).eq('tblSchemaTest');
    expect(schema.columns).has.any.keys(['ID']);
    expect(schema.columns).has.any.keys(['name']);
  });

  it('Can update schema:', async () => {
    const store = new MySqlStore(connectionSettings);
    await store.execute('DROP TABLE IF EXISTS tblTest');
    await store.execute('CREATE TABLE tblTest (ID INT auto_increment, name VARCHAR(1), PRIMARY KEY (ID))');

    const repos = new TestRepository();

    await store.checkSchema(repos);

    const newSchema = await store.parseSchema('tblTest');

    expect(newSchema.columns.name).to.equal('`name` varchar(100) NOT NULL DEFAULT \'\'');
    expect(newSchema.columns.email).to.equal('`email` varchar(200) NOT NULL DEFAULT \'\'');
  });

  it('Knows the types of pull ups', async () => {
    const store = new MySqlStore(connectionSettings);
    const orderRepos = new TestOrdersRepository();
    const contactRepos = new TestContactsRepository();
    store.addRepository(orderRepos);
    store.addRepository(contactRepos);

    const contact = new TestContactEntity();
    await contactRepos.store(contact);

    const order = new TestOrderEntity();
    order.completed = true;
    order.contactId = contact.id;
    await orderRepos.store(order);

    const contacts = contactRepos.all().intersectWith(
        orderRepos.all(),
        'id',
        'contactId',
      {
        completed: 'complete',
      },
    );

    await contacts.fetch();

    expect(contacts.item(0).complete).to.be.equal(true);
  });

  // @ts-ignore
  testStore(async () => {
    const store = new MySqlStore(connectionSettings);
    try {
      await store.execute('TRUNCATE TABLE tblTest');
    } catch (e) {}
    try {
      await store.execute('TRUNCATE TABLE tblOrders');
    } catch (e) {}
    return store;
  });

  after(() => {
    MySqlStore.closeAllConnections();
  });
});
