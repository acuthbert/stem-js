import 'mocha';
import { Entity } from '../../src/Entities/Entity';
import { Repository } from '../../src/Repositories/Repository';
import { isUniqueIdentifier } from '../../src/Entities/Decorators/isUniqueIdentifier';
import { autoincrement } from '../../src/Entities/Decorators/autoincrement';
import { columnName } from '../../src/Entities/Decorators/columnName';
import { column } from '../../src/Entities/Decorators/column';
import { IntegerColumn } from '../../src/Schema/Columns/IntegerColumn';
import { StringColumn } from '../../src/Schema/Columns/StringColumn';
import { repositoryName } from '../../src/Repositories/repositoryName';
import { expect } from 'chai';
import { AndGroupFilter } from '../../src/Collections/Filters/AndGroupFilter';
import { EqualsFilter } from '../../src/Collections/Filters/EqualsFilter';
import { Store } from '../../src/Stores/Store';
import { SortDirection } from '../../src/Collections/Sort';
import { BooleanColumn } from '../../src/Schema/Columns/BooleanColumn';
import { SumOfAggregate } from '../../src/Collections/Aggregates/SumOfAggregate';
import { AverageAggregate } from '../../src/Collections/Aggregates/AverageAggregate';
import { Dayjs } from 'dayjs';
import { DateColumn } from '../../src/Schema/Columns/DateColumn';
import dayjs = require('dayjs');
import {CountAggregate} from '../../src/Collections/Aggregates/CountAggregate';
import {MinimumAggregate} from '../../src/Collections/Aggregates/MinimumAggregate';
import {MaximumAggregate} from '../../src/Collections/Aggregates/MaximumAggregate';
import {CountDistinctAggregate} from '../../src/Collections/Aggregates/CountDistinctAggregate';

export class TestContactEntity extends Entity {
  @isUniqueIdentifier()
    @autoincrement()
    @columnName('ID')
    @column(new IntegerColumn())
    id: number = 0;

  @column(new StringColumn(100, ''))
    name: string = '';

  @column(new StringColumn(200, ''))
    email: string = '';

  @column(new DateColumn())
  dob?: Dayjs;
}

@repositoryName('tblTest')
export class TestContactsRepository extends Repository<TestContactEntity> {
  public getEntityType(): any {
    return TestContactEntity;
  }
}

export class TestOrderEntity extends Entity {
  @isUniqueIdentifier()
  @autoincrement()
  @columnName('ID')
  @column(new IntegerColumn())
  id: number = 0;

  @column(new IntegerColumn())
  contactId: number = 0;

  @column(new StringColumn(100, ''))
  reference: string = '';

  @column(new BooleanColumn())
  completed: boolean = false;

  @column(new IntegerColumn())
  value: number = 0;
}

@repositoryName('tblOrders')
export class TestOrdersRepository extends Repository<TestOrderEntity> {
  public getEntityType(): any {
    return TestOrderEntity;
  }
}

export function testStore(storeFactory: () => Promise<Store>) {

  beforeEach(async function (this: any) {
    this.store = await storeFactory();
    this.repos = new TestContactsRepository();
    this.store.addRepository(this.repos);
  });

  it('Can store and fetch items correctly', async function (this:any) {
    let list = await this.store.fetchItems(this.repos, new AndGroupFilter(), [], [], [], []);
    expect(list.totalLength).eq(0);

    let entity = new TestContactEntity();
    entity.name = 'Mary';

    await this.store.storeEntity(entity, this.repos);

    list = await this.store.fetchItems(this.repos, new AndGroupFilter(), [], [], [], []);
    expect(list.totalLength).eq(1);
    expect(list.item(0)).instanceOf(TestContactEntity);
    expect(list.item(0).name).equals('Mary');
    expect(list.item(0).id).equals(1);

    expect(entity.id).equals(1);

    entity = new TestContactEntity();
    entity.name = 'John';

    await this.store.storeEntity(entity, this.repos);

    list = await this.store.fetchItems(this.repos, new EqualsFilter('name', 'John'), [], [], [], []);

    expect(list.totalLength).eq(1);
    expect(list.item(0)).instanceOf(TestContactEntity);
    expect(list.item(0).name).equals('John');
    expect(list.item(0).id).equals(2);

    entity.email = 'jsmith@hotmail.com';
    await this.repos.store(entity);

    list = await this.store.fetchItems(this.repos, new EqualsFilter('name', 'John'), [], [], [], []);

    expect(list.totalLength).eq(1);
    expect(list.item(0).name).equals('John');
    expect(list.item(0).email).equals('jsmith@hotmail.com');
    expect(list.item(0).id).equals(2);

  });

  it('Retrieval of non existent item throws exception', async function (this:any) {
    try {
      await this.store.getSingleEntity(TestContactEntity, 2);
            // @ts-ignore
      this.fail('Should not have got here');
    } catch (e) {
    }
  });

  it('Can sort items', async function (this:any) {
    let item = new TestContactEntity();
    item.name = 'Jim';
    await this.repos.store(item);

    item = new TestContactEntity();
    item.name = 'Andrea';
    item.email = 'b';
    await this.repos.store(item);

    item = new TestContactEntity();
    item.name = 'Andrea';
    item.email = 'a';
    await this.repos.store(item);

    const orderedItems = this.repos.all()
        .sortBy('name')
        .sortBy('email', SortDirection.Descending);

    await orderedItems.fetch();

    expect(orderedItems.item(0).name).eq('Andrea');
    expect(orderedItems.item(0).email).eq('b');
  });

  it('Can support intersections to filter', async function (this:any) {
    const contacts = new TestContactsRepository();
    const orders = new TestOrdersRepository();

    this.store.addRepository(contacts);
    this.store.addRepository(orders);

    const contact = new TestContactEntity();
    contact.name = 'a';
    await contacts.store(contact);

    const contactsList = contacts.all().intersectWith(
        orders.all(),
        'id',
        'contactId',
    );

    await contactsList.fetch();

    expect(await contactsList.count()).eq(0);

    let order = new TestOrderEntity();
    order.reference = 'o1';
    await orders.store(order);

    contactsList.invalidate();
    await contactsList.fetch();

    expect(await contactsList.count()).eq(0);

    order = new TestOrderEntity();
    order.reference = 'o2';
    order.contactId = contact.id;
    await orders.store(order);

    contactsList.invalidate();
    await contactsList.fetch();

    expect(await contactsList.count()).eq(1);
  });

  it('Can join without filtering', async function (this:any) {
    const contacts = new TestContactsRepository();
    const orders = new TestOrdersRepository();

    this.store.addRepository(contacts);
    this.store.addRepository(orders);

    const contact = new TestContactEntity();
    contact.name = 'a';
    await contacts.store(contact);

    const contactsList = contacts.all().joinWith(
        orders.all(),
        'id',
        'contactId',
    );

    await contactsList.fetch();

    expect(await contactsList.count()).eq(1);
  });

  it('Can pull up values', async function (this:any) {
    const contacts = new TestContactsRepository();
    const orders = new TestOrdersRepository();

    this.store.addRepository(contacts);
    this.store.addRepository(orders);

    const contact = new TestContactEntity();
    contact.name = 'a';
    await contacts.store(contact);

    const order = new TestOrderEntity();
    order.reference = 'o1';
    order.contactId = contact.id;
    await orders.store(order);

    const contactsList = contacts.all().intersectWith(
        orders.all(),
        'id',
        'contactId', {
          reference: 'reference',
        },
    );

    await contactsList.fetch();
    expect(contactsList.item(0).reference).equals('o1');
  });

  it('Can be grouped', async function (this:any) {
    const orders = new TestOrdersRepository();
    this.store.addRepository(orders);

    let order = new TestOrderEntity();
    order.reference = 'o1';
    order.contactId = 1;
    await orders.store(order);

    order = new TestOrderEntity();
    order.reference = 'o2';
    order.contactId = 1;
    await orders.store(order);

    order = new TestOrderEntity();
    order.reference = 'o3';
    order.contactId = 2;
    await orders.store(order);

    let ordersList = orders.all().groupBy('contactId');
    await ordersList.fetch();

    expect(ordersList.count()).equals(2);
    expect(ordersList.item(0).contactId).equals(1);
    expect(ordersList.item(1).contactId).equals(2);

    order = new TestOrderEntity();
    order.reference = 'o3';
    order.contactId = 2;
    await orders.store(order);

    ordersList = orders.all()
        .groupBy('contactId')
        .groupBy('reference');

    await ordersList.fetch();

    expect(ordersList.count()).equals(3);

    expect(ordersList.item(0).reference).equals('o1');
    expect(ordersList.item(1).reference).equals('o2');
    expect(ordersList.item(2).reference).equals('o3');
    expect(ordersList.item(2).contactId).equals(2);
  });

  it('Can aggregate', async function (this: any) {
    const orders = new TestOrdersRepository();
    this.store.addRepository(orders);

    let order = new TestOrderEntity();
    order.value = 1;
    await orders.store(order);

    order = new TestOrderEntity();
    order.value = 2;
    await orders.store(order);

    order = new TestOrderEntity();
    order.value = 2;
    await orders.store(order);

    order = new TestOrderEntity();
    order.value = 3;
    await orders.store(order);

    const collection = orders.all().addAggregate(
        new SumOfAggregate('value', 'SumOfValue'),
        new AverageAggregate('value', 'AverageOfValue'),
        new CountAggregate('value', 'CountOfValue'),
        new CountDistinctAggregate('value', 'CountDistinctOfValue'),
        new MinimumAggregate('value', 'MinimumOfValue'),
        new MaximumAggregate('value', 'MaximumOfValue'),
    );

    await collection.fetch();

    expect(collection.item(0).SumOfValue).to.equal(8);
    expect(collection.item(0).AverageOfValue).to.equal(2);
    expect(collection.item(0).CountOfValue).to.equal(4);
    expect(collection.item(0).CountDistinctOfValue).to.equal(3);
    expect(collection.item(0).MinimumOfValue).to.equal(1);
    expect(collection.item(0).MaximumOfValue).to.equal(3);
  });

  it('Supports dates', async function (this: any) {
    const contacts = new TestContactsRepository();

    this.store.addRepository(contacts);

    const contact = new TestContactEntity();
    contact.name = 'a';
    contact.dob = dayjs('2017-01-02');
    await contacts.store(contact);

    const loadedContact = await contacts.one({ id: contact.id });
    expect(loadedContact.dob).to.not.equal(undefined);
    if (loadedContact.dob) {
      expect(loadedContact.dob.format('DD MM YYYY')).to.equal('02 01 2017');
    }
  });

  it('Supports deleting entities', async function (this: any) {
    const contacts = new TestContactsRepository();

    this.store.addRepository(contacts);

    const contact = new TestContactEntity();
    contact.name = 'a';
    await contacts.store(contact);

    await contacts.delete(contact);

    const contactList = contacts.all();
    await contactList.fetch();
    expect(contactList.count()).to.equal(0);
  });
}
