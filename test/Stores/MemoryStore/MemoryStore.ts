import { expect } from 'chai';
import 'mocha';
import { Entity } from '../../../src/Entities/Entity';
import { Repository } from '../../../src/Repositories/Repository';
import { MemoryStore } from '../../../src/Stores/MemoryStore/MemoryStore';
import { testStore } from '../Store';

class TestEntity extends Entity {
  name: string = '';
}

class TestRepository extends Repository<TestEntity> {
  public getEntityType(): any {
  }

}

class Test2Entity extends Entity {
  name: string = '';
  testEntityId: number = 0;
}

class Test2Repository extends Repository<Test2Entity> {
  public getEntityType(): any {
  }
}

describe('MemoryStore', () => {
  const store = new MemoryStore();
  const repos = new TestRepository();

  store.addRepository(repos);

  it('Can store items', () => {
    const entity = new TestEntity();

    store.storeEntity(entity, repos);
    // No crash means it was received
  });

  it('Can retrieve items', async () => {
    const entity = new TestEntity();
    entity.id = 1;
    store.storeEntity(entity, repos);

    let result = await store.getSingleEntity(TestEntity, 1);

    expect(result.id).eq(1);

    entity.id = 2;
    entity.name = 'test';
    await store.storeEntity(entity, repos);

    result = await store.getSingleEntity(TestEntity, 2);

    expect(result.id).eq(2);
    expect(result.name).eq('test');
  });

  testStore(async () => {
    return new MemoryStore();
  });
});
