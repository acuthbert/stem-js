import { Repository } from '../../src/Repositories/Repository';
import { UnitTestContactEntity } from './UnitTestContactEntity';
import { repositoryName } from '../../src/Repositories/repositoryName';

@repositoryName('tblContacts')
export class UnitTestContactsRepository extends Repository<UnitTestContactEntity> {
  public getEntityType() {
    return UnitTestContactEntity;
  }
}
