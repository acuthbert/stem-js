import { MemoryStore } from '../../src/Stores/MemoryStore/MemoryStore';
import { Entity } from '../../src/Entities/Entity';
import { Repository } from '../../src/Repositories/Repository';

export class UnitTestStore extends MemoryStore {

  public lastStored?: Entity;

  async storeEntity<T extends Entity>(entity: T, repository: Repository<T>): Promise<T> {
    this.lastStored = entity;
    return super.storeEntity(entity, repository);
  }
}
