import { UnitTestContactsRepository } from './UnitTestContactsRepository';
import { MySqlConnectionSettings } from '../../src/Stores/MySqlStore/MySqlConnectionSettings';
import { MySqlStore } from '../../src/Stores/MySqlStore/MySqlStore';

const connection = new MySqlConnectionSettings();
connection.user = 'unit-testing';
connection.password = 'unit-testing';
connection.database = 'unit-testing';
connection.host = 'localhost';

const store = new MySqlStore(connection);

store.addRepository(new UnitTestContactsRepository());

export { store };
