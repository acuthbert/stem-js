import { Entity } from '../../src/Entities/Entity';
import { column } from '../../src/Entities/Decorators/column';
import { StringColumn } from '../../src/Schema/Columns/StringColumn';
import {autoincrement} from "../../src/Entities/Decorators/autoincrement";
import {isUniqueIdentifier} from "../../src/Entities/Decorators/isUniqueIdentifier";
import {IntegerColumn} from "../../src/Schema/Columns/IntegerColumn";

export class UnitTestContactEntity extends Entity {

  @autoincrement()
  @isUniqueIdentifier()
  @column(new IntegerColumn())
  id: string|number = '';

  @column(new StringColumn(100, ''))
  forename = '';

  @column(new StringColumn(100, ''))
  surname = '';
}
