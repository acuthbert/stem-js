import {Singleton} from "../../src/Shared/Singleton";
import {expect} from 'chai';
import 'mocha';

var x = 0;

class SingletonTest extends Singleton {
    public c: any;
    protected constructor() {
        super();
        this.c = x;
        x++;
    }

    public static singleton(): SingletonTest {
        return this.getInstance(SingletonTest, function(){
            return new SingletonTest();
        });
    }
}

describe('Singleton', function() {
    it('Returns an instance', function() {
        let i = SingletonTest.singleton();
        expect(i).instanceOf(SingletonTest);
    });

    it('Returns the same instance', function() {
        let i = SingletonTest.singleton();
        expect(i.c).eq(0);
    });
});
