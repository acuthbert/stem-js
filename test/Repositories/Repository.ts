import { expect } from 'chai';
import 'mocha';
import { Repository } from '../../src/Repositories/Repository';
import { Entity } from '../../src/Entities/Entity';
import { IntegerColumn } from '../../src/Schema/Columns/IntegerColumn';
import { isUniqueIdentifier } from '../../src/Entities/Decorators/isUniqueIdentifier';
import { StringColumn } from '../../src/Schema/Columns/StringColumn';
import { Column } from '../../src/Schema/Columns/Column';
import { column } from '../../src/Entities/Decorators/column';
import { ColumnType } from '../../src/Schema/Columns/ColumnType';
import { UnitTestStore } from '../Fixtures/UnitTestStore';
import { fail } from 'assert';
import { OrGroupFilter } from '../../src/Collections/Filters/OrGroupFilter';
import { EqualsFilter } from '../../src/Collections/Filters/EqualsFilter';

class TestEntity extends Entity {
  @isUniqueIdentifier()
  @column(new IntegerColumn())
  ID = 0;

  @column(new StringColumn(100, ''))
  name = '';
}

class TestRepository extends Repository<TestEntity> {
  public getEntityType(): any {
    return TestEntity;
  }
}

describe('Repository', () => {
  it('Can make new entities', () => {
    const repos = new TestRepository();
    const newEntity = repos.newEntity();

    expect(newEntity).instanceOf(TestEntity);
  });

  it('Can enumerate columns on an entity', () => {
    const repository = new TestRepository();
    const columns = repository.getStorageColumns();

    expect(Object.values(columns)).length(2);
    expect(Object.values(columns)[0]).instanceOf(Column);
    expect(Object.values(columns)[1].type).equals(ColumnType.String);
  });

  it('Can store items', async () => {
    const repository = new TestRepository();
    const entity = new TestEntity();

    const store = new UnitTestStore();

    entity.name = 'Test Name';
    entity.ID = 1;
    repository.store(entity);

    expect(store.lastStored).to.equal(entity);
  });

  it('Can find all items', async () => {
    const repository = new TestRepository();
    let entity = new TestEntity();

    const store = new UnitTestStore();

    entity.name = 'Test 1';
    entity.id = 1;
    repository.store(entity);

    let collection = repository.all();

    expect(await collection.count()).to.equal(1);

    entity = new TestEntity();
    entity.name = 'Test 2';
    entity.id = 2;
    repository.store(entity);

    collection = repository.all();

    expect(await collection.count()).to.equal(2);

    for await(const collectionEntity of collection) {
      expect(collectionEntity.name).to.equal('Test 1');
      break;
    }

    expect(collection.item(1).name).to.equal('Test 2');
  });

  it('Throws when accessed as an array but not fetched', async () => {
    const repository = new TestRepository();
    const store = new UnitTestStore();

    const collection = repository.all();

    try {
      collection.item(0).name;
      fail();
    } catch (e) {
    }
  });

  it('Can filter', async () => {
    const repository = new TestRepository();
    const store = new UnitTestStore();

    let entity = new TestEntity();
    entity.name = 'John';
    entity.id = 1;
    repository.store(entity);

    entity = new TestEntity();
    entity.name = 'James';
    entity.id = 2;
    repository.store(entity);

    entity = new TestEntity();
    entity.name = 'Bob';
    entity.id = 3;
    repository.store(entity);

    entity = new TestEntity();
    entity.name = 'Bob';
    entity.id = 4;
    repository.store(entity);

    let collection = repository.all();

    collection.where('name').equals('James');

    await collection.fetch();

    expect(await collection.count()).equals(1);
    expect(collection.item(0).name).to.equal('James');

    collection = repository.all();

    collection.where('name').equals('Bob');
    await collection.fetch();

    expect(await collection.count()).equals(2);
    expect(collection.item(0).name).to.equal('Bob');
  });

  it('Supports OR groups', async () => {
    const repository = new TestRepository();
    const store = new UnitTestStore();

    let entity = new TestEntity();
    entity.name = 'John';
    entity.id = 1;
    repository.store(entity);

    entity = new TestEntity();
    entity.name = 'James';
    entity.id = 2;
    repository.store(entity);

    entity = new TestEntity();
    entity.name = 'Bob';
    entity.id = 3;
    repository.store(entity);

    const collection = repository.all();

    collection.where(new OrGroupFilter()
      .where('name').equals('James')
      .or('name').equals('John'));
    await collection.fetch();

    expect(await collection.count()).equals(2);
  });

  it('Can use where on a repository as well as a collection', async () => {
    const repository = new TestRepository();
    const store = new UnitTestStore();

    const entity = new TestEntity();
    entity.name = 'John';
    entity.id = 1;
    repository.store(entity);

    const collection = repository.where('name').equals('John');
    await collection.fetch();

    expect(await collection.count()).equals(1);
    expect(collection.item(0).name).equals('John');
  });

  it('Can return 1 item by dictionary', async () => {
    const repository = new TestRepository();
    const store = new UnitTestStore();

    let entity = new TestEntity();
    entity.name = 'John';
    entity.id = 1;
    repository.store(entity);

    entity = new TestEntity();
    entity.name = 'James';
    entity.id = 2;
    repository.store(entity);

    const james = await repository.one({ name:  'James' });

    expect(james.id).eq(entity.id);
    expect(james.name).eq('James');
  });

  it('Can return 1 item by dictionary', async () => {
    const repository = new TestRepository();
    const store = new UnitTestStore();

    const entity = new TestEntity();
    entity.name = 'mary';
    entity.id = 1;
    repository.store(entity);

    const mary = await repository.one(new EqualsFilter('name', 'mary'));

    expect(mary.id).eq(entity.id);
  });
});
