#!/usr/bin/env ts-node

const promise = new Promise((resolve, reject) => {
  const readline = require('readline');

  if (process.argv.length > 2) {
    resolve(process.argv[2]);
    return;
  }

  const rl = readline.createInterface(process.stdin, process.stdout);

  rl.question('Enter the path to a valid bootstrap js file: ', (path: string) => {
    resolve(path);
  });
});

promise.then(async (path) => {
  const store = require(path.toString()).store;

  await store.checkAllSchemas();

  process.exit(0);
});
